﻿using System;

namespace CSharpBasics.Utilities
{
	public class ShapeCalculator
	{
		/// <summary>
		/// Возвращает площадь прямоугольника со сторонами <see cref="a"/> и <see cref="b"/>
		/// </summary>
		/// <param name="a">Длина стороны a прямоугольника</param>
		/// <param name="b">Длина стороны b прямоугольника</param>
		/// <returns>Площадь прямоугольника</returns>
		/// <exception cref="ArgumentOutOfRangeException">
		/// Выбрасывается, если <see cref="a"/> или <see cref="b"/> меньше или равны нулю
		/// </exception>
		public float CalcRectangleArea(int a, int b)
		{
			if (a <= 0)
            {
				throw new ArgumentOutOfRangeException("a", "a is less than or equal to 0");
			}

			if (b <= 0)
			{
				throw new ArgumentOutOfRangeException("b", "b is less than or equal to 0");
			}

			return a * b;
		}
	}
}