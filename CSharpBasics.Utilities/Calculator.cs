﻿using System;

namespace CSharpBasics.Utilities
{
	public class Calculator
	{
		/// <summary>
		/// Вычисляет сумму всех натуральных чисел меньше <see cref="number"/>, кратных любому из <see cref="divisors"/>
		/// </summary>
		/// <param name="number">Натуральное число</param>
		/// <param name="divisors">Числа, которым должны быть кратны слагаемые искомой суммы</param>
		/// <returns>Вычисленная сумма</returns>
		/// <exception cref="ArgumentOutOfRangeException">
		/// Выбрасывается в случае, если <see cref="number"/> или любое из чисел в <see cref="divisors"/> не является натуральным,
		/// а также если массив <see cref="divisors"/> пустой
		/// </exception>
		public float CalcSumOfDivisors(int number, params int[] divisors)
		{
			if (number <= 0)
            {
				throw new ArgumentOutOfRangeException("number", "number is less than or equal to 0");
            }

			if (divisors.Length == 0)
			{
				throw new ArgumentOutOfRangeException("divisors", "There is not any divisor");
			}

			int sumOfDivisors = 0;
			for (int i = 1; i < number; ++i)
            {
				for (int j = 0; j < divisors.Length; ++j)
                {
					if (divisors[j] <= 0)
					{
						throw new ArgumentOutOfRangeException("divisors", "Devisor is less than 0");
					}

					if (i % divisors[j] == 0)
                    {
						sumOfDivisors += i;
                    }
                }
            }

			return sumOfDivisors;
		}

		/// <summary>
		/// Возвращает ближайшее наибольшее целое, состоящее из цифр исходного числа <see cref="number"/>
		/// </summary>
		/// <param name="number">Исходное число</param>
		/// <returns>Ближайшее наибольшее целое</returns>
		public long FindNextBiggerNumber(int number)
		{
			if (number < 1)
			{
				throw new ArgumentOutOfRangeException("number", "Number is less than 1");
			}

			char[] digits = number.ToString().ToCharArray();

			Array.Reverse(digits);

			for (int i = 1; i < digits.Length; ++i)
			{
				if (digits[i] < digits[i - 1])
				{
					(digits[i - 1], digits[i]) = (digits[i], digits[i - 1]);
					Array.Reverse(digits);
					Array.Sort(digits, digits.Length - i, i);
					break;
				}
			}

			long nextBiggerNumber = Convert.ToInt64(new string(digits));

			if (nextBiggerNumber <= number)
			{
				throw new InvalidOperationException();
			}

			return nextBiggerNumber;
		}
    }
}