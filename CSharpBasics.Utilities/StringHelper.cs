﻿using System;
using System.Text;

namespace CSharpBasics.Utilities
{
	public class StringHelper
	{
		/// <summary>
		/// Вычисляет среднюю длину слова в переданной строке без учётов знаков препинания и специальных символов
		/// </summary>
		/// <param name="inputString">Исходная строка</param>
		/// <returns>Средняя длина слова</returns>
		public int GetAverageWordLength(string inputString)
		{
            if (inputString is null)
            {
                return 0;
            }

            string[] words = inputString.Split(' ');
            int[] wordLengths = new int[words.Length];

            int numLetters = 0;
            int numWords = 0;
            for (int i = 0; i < words.Length; ++i)
            {
                for (int j = 0; j < words[i].Length; ++j)
                {
                    if (char.IsLetter(words[i][j]))
                    {
                        wordLengths[i]++;
                    }
                }

                if (wordLengths[i] > 0)
                {
                    numLetters += wordLengths[i];
                    numWords++;
                }
            }

            return numWords > 0 ? numLetters / numWords : 0;
        }

		/// <summary>
		/// Удваивает в строке <see cref="original"/> все буквы, принадлежащие строке <see cref="toDuplicate"/>
		/// </summary>
		/// <param name="original">Строка, символы в которой нужно удвоить</param>
		/// <param name="toDuplicate">Строка, символы из которой нужно удвоить</param>
		/// <returns>Строка <see cref="original"/> с удвоенными символами, которые есть в строке <see cref="toDuplicate"/></returns>
		public string DuplicateCharsInString(string original, string toDuplicate)
		{
            if (original is null || toDuplicate is null)
            {
                return original;
            }

            StringBuilder sb = new StringBuilder();
            string toDuplicateLower = toDuplicate.ToLower();

            foreach (char ch in original)
            {
                sb.Append(ch);
                if (toDuplicateLower.Contains(char.ToLower(ch)))
                {
                    sb.Append(ch);
                }
            }

            return sb.ToString();
		}
	}
}