﻿using System;

namespace CSharpBasics.Utilities
{
	public class ArrayHelper
	{
		/// <summary>
		/// Вычисляет сумму неотрицательных элементов в одномерном массиве
		/// </summary>
		/// <param name="numbers">Одномерный массив чисел</param>
		/// <returns>Сумма неотрицательных элементов массива</returns>
		/// <exception cref="ArgumentNullException">Выбрасывается, если <see cref="numbers"/> равен null</exception>
		public float CalcSumOfPositiveElements(int[] numbers)
		{
			if (numbers is null)
            {
				throw new ArgumentNullException(nameof(numbers));
            }

			int sumOfPositiveElements = 0;
			for (int i = 0; i < numbers.Length; ++i)
            {
				if (numbers[i] > 0)
                {
					sumOfPositiveElements += numbers[i];
                }
            }

			return sumOfPositiveElements;
		}

		/// <summary>
		/// Заменяет все отрицательные элементы в трёхмерном массиве на нули
		/// </summary>
		/// <param name="numbers">Массив целых чисел</param>
		/// <exception cref="ArgumentNullException">Выбрасывается, если <see cref="numbers"/> равен null</exception>
		public void ReplaceNegativeElementsBy0(int[,,] numbers)
		{
			if (numbers is null)
			{
				throw new ArgumentNullException(nameof(numbers));
			}

			int firstDimentionLength = numbers.GetLength(0);
			int secondDimentionLength = numbers.GetLength(1);
			int thirdDimentionLength = numbers.GetLength(2);

			for (int i = 0; i < firstDimentionLength; ++i)
            {
				for (int j = 0; j < secondDimentionLength; ++j)
                {
					for (int k = 0; k < thirdDimentionLength; ++k)
                    {
						if (numbers[i, j, k] < 0)
                        {
							numbers[i, j, k] = 0;
						}	
                    }
                }
            }
		}

		/// <summary>
		/// Вычисляет сумму элементов двумерного массива <see cref="numbers"/>,
		/// которые находятся на чётных позициях ([1,1], [2,4] и т.д.)
		/// </summary>
		/// <param name="numbers">Двумерный массив целых чисел</param>
		/// <returns>Сумма элементов на четных позициях</returns>
		/// <exception cref="ArgumentNullException">Выбрасывается, если <see cref="numbers"/> равен null</exception>
		public float CalcSumOfElementsOnEvenPositions(int[,] numbers)
		{
			if (numbers is null)
			{
				throw new ArgumentNullException(nameof(numbers));
			}

			int sumOfElementsOnEvenPositions = 0;
			int firstDimentionLength = numbers.GetLength(0);
			int secondDimentionLength = numbers.GetLength(1);

			for (int i = 0; i < firstDimentionLength; ++i)
			{
				for (int j = 0; j < secondDimentionLength; ++j)
				{
					if ((i + j) % 2 == 0)
					{
						sumOfElementsOnEvenPositions += numbers[i, j];
					}
				}
			}

			return sumOfElementsOnEvenPositions;
		}

		/// <summary>
		/// Фильтрует массив <see cref="numbers"/> таким образом, чтобы на выходе остались только числа, содержащие цифру <see cref="filter"/>
		/// </summary>
		/// <param name="numbers">Массив целых чисел</param>
		/// <param name="filter">Цифра для фильтрации массива <see cref="numbers"/></param>
		/// <returns>Массив отфильтрованных целых чисел или пустой массив, если <see cref="numbers"/> равно null</returns>
		public int[] FilterArrayByDigit(int[] numbers, byte filter)
		{
			if (numbers is null)
            {
				return new int[] { };
            }

			return Array.FindAll(numbers, n => n.ToString().Contains(filter.ToString()));
		}
	}
}